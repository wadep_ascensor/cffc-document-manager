{{render('common.header')}}
<div class="container containerie7">
      <h1>Add {{$context}}</h1>
	  @if($errors->has())
	<div class="alert alert-danger">
		{{$errors->first('categoryname','<span >:message</span>')}}<br>
	</div>
	@endif
      <div class="well">
		{{Form::open('category/create','POST',array('class' => 'form-vertical form-custom clearfix'))}}
			<div class="clearfix">
			  <div class="control-group pull-left">
				<label for="name" class="control-label">{{FORM::label('category_name',$context.' Name')}}</label>
				<div class="controls-row">
				  {{Form::text('categoryname',Input::old('categoryname'),array('class' => 'span6'))}}
				</div>
				<label for="username" class="control-label">{{FORM::label('user_name','Users who can view this '.Category::$context)}}</label>
				<div class="controls-row">
					@foreach($users as $user)
					<label class="checkbox">
    					<input type="checkbox"
    						   name="User{{$user->id}}"
    						   id="User{{$user->id}}"
    						   value="1" /><strong>{{$user->username}}</strong>
    				</label>
    				@endforeach
			  	</div>
			  </div>
		  </div>
		{{Form::submit('Save '.$context, array('class' => 'btn btn-primary pull-left'))}}<br />
         <!--<a class="btn btn-primary" href="/view-members.php"><span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span></a>-->
      </div>
 {{Form::close()}}    </div>
{{render('common.footer')}}