{{render('common.header')}}
<div class="container containerie7">
      <h1>{{Str::plural($context)}}</h1>
	  <br>
	  <br>
	  @if(Session::has('message') && Session::has('mode'))
		<!-- Previous action executed -->
		<div class="alert alert-{{Session::get('mode')}}">
			<a data-dismiss="alert" class="close">×</a>
			<span>{{Session::get('message')}}.</span>
		</div>
	@endif
	   <table border="0" width="100%">
	   <tr>
	           @if($group = Sentry::group($groupid_login)->get('name')=='Admin')<td align="right" width="10%"><div>            
			  <a href="{{URL::base()}}/category/new" class="btn btn-small btn-primary pull-right">Add {{$context}}</a></div></td> @endif
	   </tr>
	   </table>
	 <br />
      <div class="well">
        <div class="well well-white push-down">
          <table class="table zebra-striped">
            <tbody>
              <tr>
                <!--<th class="align-left">Id</th>-->
                <th class="align-left">{{$context}} Name</th>
                 @if($group = Sentry::group($groupid_login)->get('name')=='Admin')
               <th>Edit/Delete</th>
			    @endif
              </tr>
			  @foreach($categories as $category)

              <tr>
                <!--<td class="align-left"><span class="length-control">{{$category->id}}</span></td>-->
                <td class="align-left"><span>{{$category->categoryname}}</span></td>
				 @if($group = Sentry::group($groupid_login)->get('name')=='Admin')
               <td>
                  <div class="btn-group">
                    <a href="{{URL::base()}}/category/{{$category->id}}/edit" class="btn btn-mini btn-warning">
                    	<i class="icon-cog icon-white"></i>
                    </a>
                    <a href="#" class="btn btn-mini btn-warning dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li><a href="{{URL::base()}}/category/{{$category->id}}/edit"><i class="icon-pencil"></i> Edit</a></li>
                      <li class="divider"></li>
                      <li><a href="{{URL::base()}}/category/{{$category->id}}/delete"><i class="icon-trash"></i> Delete</a></li>
                    </ul>
                  </div> 
                </td>
				  @endif
              </tr>
			  @endforeach
            </tbody>
          </table>
        </div>
		<div class="pagination pagination-right">
			{{$orders->links()}}
        </div>
      </div>
    </div>
{{render('common.footer')}}