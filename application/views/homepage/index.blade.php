{{render('common.header')}}
<div class="container containerie7">
      <h1>{{Str::plural(Category::$context)}}</h1>
	  <br>
	  <br>
	  @if(Session::has('message') && Session::has('mode'))
		<!-- Previous action executed -->
		<div class="alert alert-{{Session::get('mode')}}">
			<a data-dismiss="alert" class="close">×</a>
			<span>{{Session::get('message')}}.</span>
		</div>
	@endif
	 <br />
      <div class="well">
        <div class="well well-white push-down">
          <table class="table zebra-striped">
            <tbody>
              <tr>
                <!--<th class="align-left">Id</th>-->
                <th class="align-left">{{Category::$context}} Name</th>
              </tr>
			  @foreach($categories as $category)

              <tr>
                <!--<td class="align-left"><span class="length-control">{{$category->id}}</span></td>-->
                <td class="align-left">
                  <a href="{{URL::base()}}/document?catfilter={{$category->id}}">
                    {{$category->categoryname}}
                  </a>
                </td>
              </tr>
			  @endforeach
            </tbody>
          </table>
        </div>
		<div class="pagination pagination-right">
			{{$orders->links()}}
        </div>
      </div>
    </div>
{{render('common.footer')}}