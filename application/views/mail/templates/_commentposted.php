<dl>
	<dt>
		<span>The following comment was posted on your document</span>
		<b><?php echo $document_title; ?></b>
		<span>by</span>
		<b><?php echo $username; ?></b>
		<br /> on
		<span style="color: blue;">
			<?php echo date('d F Y'); ?>
		</span><b>:</b>
	</dt>
	<dd style="color: #454545; margin-top: 20px;">
		<?php echo $comment; ?>
	</dd>
</dl>
