<dl>
	<dt>Hi <b><?php echo $username; ?></b>,</dt>
	<dd style="color: #454545; margin-top: 20px;">
		<p>You requested to reset the password for your account, here is a link the change your password:</p>
		<p><a href="<?php echo $reset_link; ?>"><?php echo $reset_link; ?></a></p>
		<p>If you did not request to reset your password, ignore this email</p>
</dd>
</dl>