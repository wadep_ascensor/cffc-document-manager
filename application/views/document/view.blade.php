{{render('common.header')}}
<!--<script type="text/javascript">
	$(document).ready( function(){
		alert("jQuery loaded and ready to use");
	});
</script>-->
<div class="container containercomments containerie7">
      <h1>{{ $document->title }}</h1>
      @if($has_permission == true)
      <div class="row">
        <div class="span12">
          <div class="well well-small clearfix">
            <!--<div class="input-append pull-left">
              <input type="text" id="appendedInputButtons" class="span3">
              <button type="button" class="btn btn-primary"><i class="icon-search icon-white"></i> Search</button>
            </div>-->
            <div class="btn-group pull-right">
            	<a href="{{URL::base()}}/document/edit/{{$document->id}}" class="btn btn-mini btn-warning"><i class="icon-cog icon-white"></i></a>
            	<a href="#" class="btn btn-mini btn-warning dropdown-toggle" data-toggle="dropdown">
                	<span class="caret"></span>
            	</a>
                <ul class="dropdown-menu">
                	<li><a class="edit-document" href="{{URL::base()}}/document/edit/{{$document->id}}"><i class="icon-pencil"></i> Edit</a></li>
                	<li class="divider"></li>
                	<li><a class="delete-document" href="{{URL::base()}}/document/delete/{{$document->id}}"><i class="icon-trash"></i> Delete</a></li>
                </ul>
          	</div> 
          </div>
        </div>
      </div>
      @endif
      
      <div class="row push-down">
        <div class="span8">
          <p>{{ $document->description }}</p>
        </div>
        <div class="span4">
        @if(file_exists(getcwd().DS."public".DS."uploads".DS.$document->image))
          <a href="{{URL::base()}}/uploads/{{$document->image}}" target="_blank" class="btn btn-large btn-block btn-success align-center"><span class="btn-label"><i class="icon-arrow-down icon-white"></i> Download</span></a>
        @endif
        </div>
      </div>
      <div class="row">
        <div class="span12">
          <h3>{{ $pag_details['records_count'] }} {{ Str::plural('Comment', $pag_details['records_count']); }}</h3>
          <div class="well clearfix">
          	{{ Form::open() }}
          	<input type="hidden" name="delete-comment" id="delete-comment" value="" /> <!-- Pass a comment to delete -->
            <div class="control-group">
              <div class="controls">
                <textarea id="comment-text" name="comment-text" rows="4"></textarea>
              </div>
            </div>
            <button id="submit-comment" type="submit" class="btn btn-primary pull-right"><span class="btn-label"><i class="icon-comment icon-white"></i> Submit Comment</span></a>
            {{ Form::close() }}
          </div>
          @forelse($comments as $comment)
          <div class="well">
			@if(Sentry::user()->id == $comment->user_id)
				{{ Form::comment_delete_button($comment->id) }}
			@elseif($is_admin == true)
				{{ Form::comment_delete_button($comment->id) }}
			@endif
            <p><i class="icon-comment"></i> Comment by 
            	@if($comment->username != '')
                	{{ $comment->username }}
                @else
                	<span style="color: red;">Deleted user</span>
                @endif 
                on {{ date("d F Y H:i:s", strtotime($comment->updated_at)) }}</p>
            <div class="well well-white push-up">
              <p>{{ $comment->comment }}</p>
            </div>
          </div>
          @empty
          <div class="well">
            <p>No one has commented yet.</p>
          </div>
          @endforelse
          <div class="pagination pagination-right">
			{{$orders->links()}}
        	</div>
        </div>
      </div>
    </div>
{{render('common.footer')}}