{{render('common.header')}}
<div class="container containerie7">
      <h1>Add/Edit Document</h1>
	  @if($errors->has())
	<div class="alert alert-danger">
		{{$errors->first('title','<span >:message</span>')}}<br>
		{{$errors->first('image','<span >:message</span >')}}<br />
		{{$errors->first('description','<span >:message</span >')}}<br />
		
	</div>
	@endif
      <div class="well">
		{{Form::open('document/create','POST',array('class' => 'form-vertical form-custom clearfix','enctype' =>'multipart/form-data'))}}
			<div class="clearfix">
			
			
			  <div class="control-group pull-left">
				<label for="name" class="control-label">{{FORM::label('title','Title')}}</label>
				<div class="controls-row">
				  {{Form::text('title',Input::old('title'),array('class' => 'span6'))}}
				</div>
			  </div>
			  
			  
			  <div class="control-group pull-left push-left">
				<label for="email" class="control-label">{{FORM::label('image','Upload File')}}</label>
				<div class="controls-row">
				 {{ Form::file('image',array('class' => 'span6')) }}
				 <!--
				 <a href="#" class="btn btn-warning pull-right"><span class="btn-label"><i class="icon-file icon-white"></i> Select File</span></a>
				 -->
				</div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="control-group pull-left">
				<label for="role" class="control-label">{{FORM::label('role',Str::plural(Category::$context))}}</label>
				<div class="controls">
				
				{{Form::select('role', $categories,'',array('class' => 'select-custom'))}}


				</div>
			  </div>
			  <div class="clearfix"></div>
			  
			  <div class="control-group pull-left">
				<label for="role" class="control-label">{{FORM::label('description','Description')}}</label>
				<div class="controls">
				{{Form::textarea('description',Input::old('description'),array('class' => 'span6 mceEditor'))}}
				</div>
			  </div>
			  
		  </div>
		<button type="submit" class="btn btn-primary pull-left"><span class="btn-label"><i class="icon-ok icon-white"></i> Save</span></button><br />
         <!--<a class="btn btn-primary" href="/view-members.php"><span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span></a>-->
      </div>
 {{Form::close()}}    </div>
{{render('common.footer')}}