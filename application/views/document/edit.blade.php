{{render('common.header')}}
<div class="container containerie7">
      <h1>Add/Edit Document</h1>
	  	  @if($errors->has())
	<div class="alert alert-danger">
		{{$errors->first('title','<span >:message</span>')}}<br>
		{{$errors->first('description','<span >:message</span >')}}
	</div>
	@endif
	@if(Session::has('message'))
	<div class="alert alert-danger">
		{{ Session::get('message') }}<br>
		
	</div>
	@endif
      <div class="well">
		{{Form::open('document/update','PUT',array('class' => 'form-vertical form-custom clearfix','enctype' =>'multipart/form-data'))}}
			<div class="clearfix">
			
			
			  <div class="control-group pull-left">
				<label for="name" class="control-label">{{FORM::label('title','Title')}}</label>
				<div class="controls-row">
				  {{Form::text('title',$document->title,array('class' => 'span6'))}}
				</div>
			  </div>
			  
			  <div class="clearfix"></div>
			  <div class="control-group pull-left">
				<label for="role" class="control-label">{{FORM::label('assignto','Assigned to (Author)')}}</label>
				<div class="controls">
				@if(Sentry::user_exists((int) $document->user_id))
				{{ Form::select('assignto', $users, $document->user_id, array('class' => 'select-custom')) }}
				@else
				{{ Form::select('assignto', $users, $user_blocked_choice, array('class' => 'select-custom')) }}
				@endif
				</div>
			  </div>
			  
			  <div class="control-group pull-left push-left">
				<label for="email" class="control-label">{{FORM::label('image','Upload File')}}</label>
				<div class="controls-row">
				 {{ Form::file('image',array('class' => 'span6')) }}
				 <div><b>Current file: </b>
				 	<a href="{{URL::base()}}/uploads/{{$document->image}}" target="_blank">{{$document->image}}</a>
				 </div>
				</div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="control-group pull-left">
				<label for="role" class="control-label">{{FORM::label('role',Str::plural(Category::$context))}}</label>
				<div class="controls">
				
				{{Form::select('role', $categories, $document->category_id, array('class' => 'select-custom'))}}

				</div>
			  </div>
			  <div class="clearfix"></div>
			  
			  <div class="control-group pull-left">
				<label for="role" class="control-label">{{FORM::label('description','Description')}}</label>
				<div class="controls">
				{{Form::textarea('description',$document->description,array('class' => 'span6 mceEditor'))}}
				</div>
			  </div>
			  
		  </div>
		  {{Form::hidden('id',$document->id)}}
		   {{Form::hidden('hid_image',$document->image)}}
		{{Form::submit('Save Document', array('class' => 'btn btn-primary pull-left'))}}<br />
         <!--<a class="btn btn-primary" href="/view-members.php"><span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span></a>-->
      </div>
 {{Form::close()}}    </div>
{{render('common.footer')}}