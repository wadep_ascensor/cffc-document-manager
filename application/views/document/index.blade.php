{{render('common.header')}}
<div class="container containerie7">
    <h1>Documents</h1>
	@if(Session::has('message') && Session::has('mode'))
		<!-- Previous action executed -->
		<div class="alert alert-{{Session::get('mode')}}">
			<a data-dismiss="alert" class="close">×</a>
			<span>{{Session::get('message')}}.</span>
		</div>
	@endif

	{{ Form::open('document', 'GET') }}
      <div class="row">
        <!-- Search and category selection -->
        <!--<div class="span6">
          <div class="input-append pull-left">
            <input 
            	type="text" 
            	name="search_query" 
            	id="appendedInputButtons" 
            	class="span3"
            	@if ($search_query != '')
            		value="{{$search_query}}"
            	@endif
            />
            <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Search</button>
          </div>
        </div>
        <div class="span6">
          <div class="control-group pull-right">
				<div class="controls">
					{{Form::select('catfilter', $categories, $cat_selected, array('class' => 'select-custom'))}}
				</div>
			  </div>
        </div>-->
      </div>
      {{ Form::close() }}
      <div class="well">
        <div class="well well-white push-down">
          <table class="table zebra-striped">
            <tbody>
              <tr>
                <th class="align-left">Document Name</th>
                <th class="align-left">Author</th>
                <th>Upload Date</th>
                <th>Comments</th>
                <th>Download</th>
                @if($group = Sentry::group($groupid_login)->get('name')=='User')
                @else
                <th>Edit/Delete</th>
                @endif
              </tr>
              @foreach($documents as $document)
              <tr>
              	<td class="align-left"><!-- Document title -->
              		<a href="{{ URL::to('document/view/').$document->id }}"
                    data-original-title="Description" 
                    rel="popover" 
                    data-content="{{Str::limit_exact(strip_tags($document->description), Config::get('ui.description_popover_length'))}}">
              			 {{Str::limit_exact($document->title, Config::get('ui.document_name_length'))}}</a>
              		</a>
              	</td>
                <td class="align-left"><!-- Document author -->
                	<span class="length-control">
                	@if($document->username != '')
                		{{ $document->username }}
                	@else
                		<span style="color: red;">Deleted user</span>
                	@endif
                	</span>
                </td>
                <td><!-- Document uploaded date -->
                	{{ date("d/m/Y", strtotime($document->updated_at)) }}
                </td>
                <td><!-- Document comments count -->
                	{{ count($document->comments()->get()) }}
                </td>
                <td>
                @if(file_exists(getcwd().DS."public".DS."uploads".DS.$document->image))
                	<a href="{{URL::base()}}/uploads/{{$document->image}}" target="_blank" class="btn btn-mini btn-success">
						<i class="icon-arrow-down icon-white"></i>
					</a>
                @else
                	<span>N/A</span>
                @endif
				</td>
				@if($group = Sentry::group($groupid_login)->get('name')=='User')
				@else
                <td>
                	@if(Sentry::user()->id==$document->user_id)
                  <div class="btn-group">
                    <a href="{{URL::base()}}/document/edit/{{$document->id}}" class="btn btn-mini btn-warning"><i class="icon-cog icon-white"></i></a>
                    <a href="#" class="btn btn-mini btn-warning dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li><a class="edit-document" href="{{URL::base()}}/document/edit/{{$document->id}}"><i class="icon-pencil"></i> Edit</a></li>
                      <li class="divider"></li>
                      <li><a class="delete-document" href="{{URL::base()}}/document/delete/{{$document->id}}"><i class="icon-trash"></i> Delete</a></li>
                    </ul>
                  </div>
                  @elseif($group = Sentry::group($groupid_login)->get('name')=='Admin')
                  <div class="btn-group">
                    <a href="{{URL::base()}}/document/edit/{{$document->id}}" class="btn btn-mini btn-warning"><i class="icon-cog icon-white"></i></a>
                    <a href="#" class="btn btn-mini btn-warning dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li><a class="edit-document" href="{{URL::base()}}/document/edit/{{$document->id}}"><i class="icon-pencil"></i> Edit</a></li>
                      <li class="divider"></li>
                      <li><a class="delete-document" href="{{URL::base()}}/document/delete/{{$document->id}}"><i class="icon-trash"></i> Delete</a></li>
                    </ul>
                  </div>
                  @endif 
                </td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- Pagination overview -->
        <dl style="display: inline;">
        @if ($pag_details['records_count'] > 0 && $pag_details['record_start_num'] != $pag_details['record_end_num'])
        	<dt>Showing results 
        		{{$pag_details['record_start_num']}}-{{$pag_details['record_end_num']}} 
        		of {{$pag_details['records_count']}}
        	</dt>
        @elseif ($pag_details['records_count'] > 0)
        	<dt>Showing result {{$pag_details['record_end_num']}} of {{$pag_details['records_count']}}</dt>
        @else
        	<dt>NO RECORDS FOUND</dt>
        @endif
        @if ($search_query != '')
        	<!-- Search and Category overview -->
        	<dd>
        		Searching for <span style="color: blue; font-weight: bold;">
        			{{$search_query}}
        		</span> in the <b>Document name</b>, <b>Author Name</b>, or <b>Description</b>
        		@if($cat_selected != 0)
        		<br />
        		In the <span style="color: blue; font-weight: bold;">
        			{{$categories[$cat_selected]}}
        		</span> {{Category::$context}}
        		@endif
        	</dd>
        @elseif ($cat_selected != 0)
        	<!-- Category overview -->
        	<dd>
        		In the <span style="color: blue; font-weight: bold;">
        			{{$categories[$cat_selected]}}
        		</span> {{Category::$context}}
        	</dd>
        @endif
        </dl>
       	<div class="pagination pagination-right">
			{{$orders->appends($get_variables)->links()}}
        </div>
        <h5><a href="{{URL::base()}}/homepage">Return to {{Str::plural(Category::$context)}} listing</a></h5>
      </div>
    </div>
{{render('common.footer')}}