<!doctype html>
<?php 
echo render('common.header');
?>
<div class="container containerie7">
      <h1>Add/Edit Member</h1>
	 <?php if($errors->has()){
	 ?>
		<div class="alert alert-danger">
		<?php
			echo $errors->first('name','<span>:message</span>')."<br>";
			echo $errors->first('email','<span>:message</span>')."<br>";
			echo $errors->first('password','<span>:message</span>')."<br>";
		?>
		</div>
	<?php  
	 }  
	?>
	<div class="well">
		 <form class="form-vertical form-custom clearfix" method="post" action="">
 			<div class="clearfix">
			  <div class="control-group pull-left">
				<label for="name" class="control-label">Name</label>
				<div class="controls-row">
				 <input class="span6" type="text" name="name" id="name"  value="<?php echo Input::old('name');?>" />
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<label for="email" class="control-label">Email</label>
				<div class="controls-row">
					<input class="span6" type="text" name="email" id="email" value="<?php echo Input::old('email');?>" />
				</div>
			  </div>
			  <div class="control-group pull-left">
				<label for="pass" class="control-label">Password</label>
				<div class="controls-row">
		        <input class="span6" type="password" name="password" id="password" placeholder="Type password here ..." value="" /><br />
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<label for="confpass" class="control-label">Confirm Password</label>
				<div class="controls-row">
				<input class="span6" type="password" name="password_confirmation" id="password_confirmation" placeholder="" value="" />
				</div>
			  </div>
			  <div class="control-group pull-left">
				<label for="role" class="control-label">Role</label>
				<div class="controls">
					<select name="role" id="role" class="select-custom">
						<option value="1">Admin</option>
						<option value="2">User</option>
						<option value="3">Author</option>
					</select>	
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<p>Status</p>
				<label class="checkbox inline">
				<input type="checkbox" name="inlineCheckbox1" id="inlineCheckbox1" value="0" />
				  Disable User
				</label>
			  </div>
			  <div class="clearfix"></div>
			  <div class="control-group push-left">
			  	<label for="categories" class="control-label"><?php echo Str::plural(Category::$context); ?> the user will have access to</label>
				<?php foreach($categories as $category): ?>
				<label class="checkbox">
    				<input type="checkbox"
    					   name="Category<?php echo $category->id; ?>"
    					   id="Category<?php echo $category->id; ?>"
    					   value="1" /><strong><?php echo $category->categoryname; ?></strong>
    			</label>
    			<?php endforeach; ?>
			  </div>
			</div>
		  <button type="submit" name="Save Member" id="Save Member" value="Save Member" class="btn btn-primary">
		  	<span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span>
		  </button>
		</form>
         <!--<a class="btn btn-primary" href="/view-members.php"><span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span></a>-->
      </div>
    </div> 
<?php echo render('common.footer');?>