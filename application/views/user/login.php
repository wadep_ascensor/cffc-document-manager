<?php echo render('common.header');?>
<div class="container containerie7">
     <h1 class="align-center push-down">Welcome to CFFC Secure</h1>
	 <?php
	// echo $errors;
	 if(is_string($errors)) { ?>
	 <div class="alert alert-danger">
		<?php echo ($errors);?></br>
	</div>
	<?php }elseif($errors->has()){ ?>
	<div class="alert alert-danger">
		<?php echo $errors->first('email','<span >:message</span>');?></br>
		<?php echo  $errors->first('password','<span >:message</span >');?></br>
	</div>
	<?php } elseif(Session::has('message') && Session::has('mode')){ ?>
		<!-- Previous action executed -->
		<div class="alert alert-<?php echo Session::get('mode'); ?>">
			<a data-dismiss="alert" class="close">×</a>
			<span><?php echo Session::get('message'); ?>.</span>
		</div>
	<?php } ?>
        <div class="well well-custom">
        	<strong>Please login to view the dashboard</strong>
		<form accept-charset="UTF-8" action="<?php echo(URL::to('user/login')); ?>" method="POST" class="form-vertical push-up">
          <div class="control-group">
            <div class="controls controls-row">
              <label for="email" class="control-label">Email Address</label>
              <input type="text" id="email" name="email" class="span7"> 
            </div>
          </div>
          <div class="control-group">
            <div class="controls controls-row">
              <label for="password" class="control-label">Password</label>
              <input type="password" id="password" name="password" class="span7">
            </div>
          </div>
          <input type="submit" value="Login" class="btn btn-primary btn-login btn-label" />
        </form>
        <p class="align-center">Forgot your password? <a href="<?php echo(URL::to('user/reset')); ?>">Click here</a>.</p>
		  </div>
      </div>
<?php echo render('common.footer');?>