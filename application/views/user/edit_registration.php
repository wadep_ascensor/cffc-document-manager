<?php 
echo render('common.header');
?>
<div class="container containerie7">
      <h1>Edit Member</h1>
	 <?php 
	// print_r($errors);
	// exit;
	 if($errors->has()){
	 ?>
		<div class="alert alert-danger">
		<?php
			echo $errors->first('name','<span>:message</span>')."<br>";
			echo $errors->first('email','<span>:message</span>')."<br>";
			echo $errors->first('password','<span>:message</span>')."<br>";
			echo $errors->first('password_confirmation','<span>:message</span>')."<br>";
		?>
		</div>
	<?php  
	 }  
	
if(Session::has('message')) { ?>
	<div class="alert alert-danger">
		<?php echo Session::get('message')  ?><br>
		
	</div>
	<?php }?>
	<div class="well">
		 <form class="form-vertical form-custom clearfix" method="post" action="">
 			<div class="clearfix">
			  <div class="control-group pull-left">
				<label for="name" class="control-label">Name</label>
				<div class="controls-row">
				 <input class="span6" type="text" name="name" id="name"  value="<?php echo $members['username'];?>" />
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<label for="email" class="control-label">Email</label>
				<div class="controls-row">
					<input class="span6" type="text" name="email" id="email" value="<?php echo $members['email'];?>" />
				</div>
			  </div>
			  <div class="control-group pull-left">
				<label for="pass" class="control-label">Password</label>
				<div class="controls-row">
		        <input class="span6" type="password" name="password" id="password" placeholder="Type password here ..." value="" /><br />
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<label for="confpass" class="control-label">Confirm Password</label>
				<div class="controls-row">
				<input class="span6" type="password" name="password_confirmation" id="password_confirmation" placeholder="" value="" />
				</div>
			  </div>
			  <div class="control-group pull-left">
				<label for="role" class="control-label">Role</label>
				<div class="controls">
					<select name="role" id="role" class="select-custom">
						<option value="1" <?php if($members['group_id'] == '1') echo "selected='selected'"; ?>>Admin</option>
						<option value="2" <?php if($members['group_id'] == '2') echo "selected='selected'"; ?>>User</option>
						<option value="3" <?php if($members['group_id'] == '3') echo "selected='selected'"; ?>>Author</option>
					</select>	
				</div>
			  </div>
			  <div class="control-group pull-left push-left">
				<p>Status</p>
				<label class="checkbox inline">
				<input type="checkbox" name="inlineCheckbox1" id="inlineCheckbox1" value="<?php  if($members['status'] == '1') echo $members['status']; else "0";?>" <?php if($members['status'] == '0') echo "checked='checked'"; ?> />
				  Disable User
				</label>
			  </div>
			  <?php if($is_admin): ?>
			  <div class="clearfix"></div>
			  <div class="control-group push-left">
			  	<label for="categories" class="control-label"><?php echo Str::plural(Category::$context); ?> the user will have access to</label>
				<?php foreach($categories as $category): ?>
				<label class="checkbox">
    				<input type="checkbox"
    					   name="Category<?php echo $category->id; ?>"
    					   id="Category<?php echo $category->id; ?>"
    					   value="1" <?php if(Member::hasCatPerms($members['id'], $category->id)){ echo 'checked '; }?>/><strong><?php echo $category->categoryname; ?></strong>
    			</label>
    			<?php endforeach; ?>
			  </div>
			<?php endif; ?>
		  </div>
		  <input type="submit" name="Save Member" id="Save Member" value="Save Member" class="btn btn-primary pull-left" />
		</form>
         <!--<a class="btn btn-primary" href="/view-members.php"><span class="btn-label"><i class="icon-ok icon-white"></i> Save Member</span></a>-->
      </div>
    </div> 
<?php echo render('common.footer');?>