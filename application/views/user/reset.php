<?php echo render('common.header'); ?>
<div class="container container-custom containerie7">
    <h1 class="align-center push-down">Reset Password</h1>
    <?php if(Session::has('message') && Session::has('mode')){ ?>
		<!-- Previous action executed -->
		<div class="alert alert-<?php echo Session::get('mode'); ?>">
			<a data-dismiss="alert" class="close">×</a>
			<span><?php echo Session::get('message'); ?>.</span>
		</div>
	<?php } elseif($errors->has()){ ?>
		<div class="alert alert-danger">
			<a data-dismiss="alert" class="close">×</a>
			<?php if(Session::has('reset')){
			 echo $errors->first('password','<span >:message</span >'); ?><br />
			 echo $errors->first('password-confirm','<span >:message</span >'); ?><br />
			<?php } else{
			echo $errors->first('email','<span >:message</span >'); ?><br />
			<?php } ?>
		</div>
	<?php } ?>
    <div class="well">
    	<strong>
    		<?php if(Session::has('reset')){ ?> 
    			Enter you new password below, and then use it to login
    		<?php } else{ ?>
    			Enter your email address to reset your password.
    		<?php } ?>
    	</strong>
    	<form class="form-vertical loginform push-up" method="post" action="
    	<?php if(Session::has('reset')){ ?>
    	<?php echo URL::base().'/user/confirmation'; /* Submit new password form */ } ?>
    	">
        	<div class="control-group">
            	<div class="controls">
            		<?php if(!Session::has('reset')){ ?> 
              		<label for="email" class="control-label">Email Address</label>
              		<input type="text" name="email" id="email" placeholder="Type email here ..." value="" class="span7"> 
              		<?php } else{ ?>
              		<label for="password" class="control-label">New password</label>
              		<input type="password" name="password" id="password" placeholder="Type new password here ..." value="" class="span7"> 
              		<label for="password-confirm" class="control-label">Confirm new password</label>
              		<input type="password" name="password-confirm" id="password-confirm" placeholder="Confirm new password ..." value="" class="span7"> 
              		<input type="hidden" name="reset" id="reset" value="<?php echo Session::get('reset'); ?> "> 
              		<?php } ?>
            	</div>
          	</div>
          	<button class="btn btn-primary" type="submit"><span class="btn-label">Submit</span></button>
    	</form>
    	<div style="text-align: center;">
    		<a href="<?php echo URL::base().'/user/login'; ?>">Go back to login page</a>
    	</div>
    </div>
</div>
<script src="/js/bootstrap.js"></script>
<?php echo render('common.footer');?>

