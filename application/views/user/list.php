<?php echo render('common.header'); ?>
<div class="container containerie7">
      <h1>Members</h1>
	  <br>
	  <br>
	  <?php
	    $hash_link = Session::get('hash_link'); //  echo $hash_link; 
		if(Session::has('message') && Session::has('mode')){
	 ?>	
		<div class="alert alert-<?php echo Session::get('mode'); ?>">
			<a data-dismiss="alert" class="close">×</a>
			<span><?php echo Session::get('message');?></span>
		 </div>
	<?php }?>
	<form method="post" action="<?php echo URL::base(); ?>/user/delete/" style="display: hidden;">
		<input type="hidden" id="delete-member" name="delete-member" value="" />
	</form>
      <div class="well">
        <div class="well well-white push-down">
          <table class="table zebra-striped">
            <tbody>
              <tr>
                <th class="align-left">Name</th>
                <th class="align-left">Email</th>
                <th>User Type</th>
				<?php if($group = Sentry::group($groupid_login)->get('name')=='Admin'){?>
                <th>Edit/Delete</th>
				<?php }?>
              </tr>
			  <?php 
			  foreach($members as $member){
				$groupid=$member->group_id;
			 	$group = Sentry::group($groupid);
			  ?>
              <tr>
                <td class="align-left"><span class="length-control"><?php echo $member->username;?></span></td>
                <td class="align-left"><span class="length-control"><?php echo $member->email;?></span></td>
                <td><?php echo $group['name'];?></td>
				<?php if($group = Sentry::group($groupid_login)->get('name')=='Admin'){?>
                <td>
                  <div class="btn-group">
                    <a href="<?php echo URL::base();?>/user/edit/<?php echo $member->id;?>"" class="btn btn-mini btn-warning"><i class="icon-cog icon-white"></i></a>
                    <a href="#" class="btn btn-mini btn-warning dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo URL::base();?>/user/edit/<?php echo $member->id;?>"><i class="icon-pencil"></i> Edit</a></li>
                      <li class="divider"></li>
                      <li><a class="delete-member" href="<?php echo URL::base();?>/user/delete/<?php echo $member->id;?>"><i class="icon-trash"></i> Delete</a></li>
                    </ul>
                  </div> 
                </td>
				<?php }?>
              </tr>
			<?php }?>
            </tbody>
          </table>
        </div>
		<div class="pagination pagination-right">
			<?php echo $orders->links(); ?>
        </div>
      </div>
    </div>
   <?php echo render('common.footer');?>