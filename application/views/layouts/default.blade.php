<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{{$title}}</title>
</head>

<body>
@if(Session::has('message'))
	<p style="color:#00FF00">{{Session::get('message')}}</p>
@endif
@yield('content')

</body>
</html>

