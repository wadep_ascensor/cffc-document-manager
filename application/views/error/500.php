<?php echo render('common.header'); ?>
<div class="container containerie7">
    <h1>Server Error: 500 (Internal Server Error)</h2>
	<h3>What does this mean?</h3>
	<p>
		Something went wrong on our servers while we were processing your request.
		We're really sorry about this, and will work hard to get this resolved as
		soon as possible.
	</p>
	<p>
		Perhaps you would like to go back to the main <?php echo HTML::link('document', 'page'); ?>?
	</p>
</div>
<?php echo render('common.footer'); ?>