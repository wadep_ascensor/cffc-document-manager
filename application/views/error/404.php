<?php echo render('common.header'); ?>
<div class="container containerie7">
    <h1>Server Error: 404 (Not Found)</h1>
	<h3>What does this mean?</h3>
	<p>
		We couldn't find the page you requested on our servers. We're really sorry
		about that. It's our fault, not yours. We'll work hard to get this page
		back online as soon as possible.
	</p>
	<p>
		Perhaps you would like to go back to the main <?php echo HTML::link('document', 'page'); ?>?
	</p>
</div>
<?php echo render('common.footer'); ?>