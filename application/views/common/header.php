<?php
$user= Session::get(Config::get('sentry::sentry.session.user'));
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="viewport" content="width=device-width">
	<link type="text/css" rel="stylesheet" href="<?php echo URL::base();?>/css/site.css" />
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
      <div class="navbar-inner">
        <div class="container">
        <?php if(Sentry::check()){?>
          <a class="brand" href="<?php echo URL::base();?>/homepage">CFFC Secure</a>
		  <div class="navbar-content">
            <ul class="nav">
              <li <?php $enroute = "document@index"; 
              	if($enroute == Request::route()->action['uses']){
			  ?>
              class="active"
              <?php } ?>
              >
                <a href="<?php echo URL::base();?>/document">View documents</a> 
              </li>
              <?php
			  	$user_group = Sentry::user($user);
				$group_name = Sentry::group($user_group->group_id)->get('name');
				if($group_name =='Admin' || $group_name == 'Author')
				{
			  ?>
              <li <?php $enroute = "document@new"; 
              	if($enroute == Request::route()->action['uses']){
			  ?>
              class="active"
              <?php } ?>
              >
                <a href="<?php echo URL::base();?>/document/new">Add Documents</a> 
              </li>
              <?php }
				if($group_name == 'Admin')
				{
			  ?>
              <li <?php $enroute = "member@list"; 
              	if($enroute == Request::route()->action['uses']){
			  ?>
              class="active"
              <?php } ?>
              >
                <a href="<?php echo URL::base();?>/member/list">View Members</a> 
              </li>
              <?php }
				if($group_name == 'Admin')
				{
			  ?>
              <li <?php $enroute = "user@register"; 
              	if($enroute == Request::route()->action['uses']){
			  ?>
              class="active"
              <?php } ?>
              >
                 <a href="<?php echo URL::base();?>/user/register">Add Member</a>
              </li>
			  <?php }
				if($group_name == 'Admin')
				{
			  ?>
			   <li <?php $enroute = "category@index"; 
              	if($enroute == Request::route()->action['uses']){
			  ?>
              class="active"
              <?php } ?>
              >
                 <a href="<?php echo URL::base();?>/category"><?php echo Str::plural(Category::$context); ?></a>
              </li>
              <?php }?>
            </ul>
          </div>
		  <div class="pull-right" style="color: #0088CC;">
		  	<div style="display: inline-block; padding-right: 5px; vertical-align: bottom; zoom:1; *display: inline;">
		  		<span>Welcome</span>
		  		<a class="userid" href="<?php echo URL::base();?>/user/edit/<?php echo Sentry::user()->id; ?>">
		  			(<?php echo Sentry::user()->username; ?>)
		  		</a>
		  	</div>
		    <a href="<?php echo URL::base();?>/user/logout" class="btn btn-small btn-primary">
		   		<i class="icon-lock icon-white"></i> Log Out
		    </a>
		  </div>
		<?php }?>
        </div>
      </div>
    </div>