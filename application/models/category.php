<?php
class Category extends Eloquent
{
	// Database table used by this object
	public static $table = 'categories';

	// Context used for naming instances of this model on frontend
	public static $context = 'Committee';
	
	// Rules for category add validation
	public static $rules = array(
		'categoryname' => 'unique:categories|required'
	);
	
	// Rules for category edit validation
	public static $rules2 = array(
		'categoryname' => 'required'
	);
	
	/**
     * Add Category Validation Start Here 
     *
	 * @param array $data Category data to validate
     * @return /Laravel/Validator
    */
	public static function validate($data)
	{
		return Validator::make($data, static::$rules);
	}
	
	/**
     * Edit Category Validation Start Here 
     *
	 * @param array $data Category data to validate
     * @return /Laravel/Validator
     */
	public static function validate1($data)
	{
		return Validator::make($data, static::$rules2);
	}
	
	
	/**
     * Edit Category Validation Start Here
     *
	 * @param array $data Category data
     * @return object
    */
	public static function editvalidate($data)
	{
		$id = $data['id'];
	    $cats = DB::table('categories')
	    			->where('categoryname', '=', $data['categoryname'])
	        		->where('id', '!=', $data['id'])
            		->get(array('id','categoryname'));
		return $cats;	
	}
	
	
	/**
     * Category pagination
     *
	 * @param int $per_page Number of records shown per page
	 * @param array $permitted_categories Categories the user is permitted to view
	 * @param boolean $permissions Set whether to use permissions or not
     * @return object
    */
	public static function pagenavquery($per_page, $permitted_categories = array(), $permissions = true)
	{
		$query = DB::table('categories');

		// Apply permissions if passed
		if (!empty($permitted_categories)) {
			$query->where_in('id', $permitted_categories);
		}
		// Return nothing if no permissions applied
		elseif ($permissions) {
			return array();
		}

		$orders = $query->paginate($per_page);
		return $orders->results;
	}	
}