<?php
class Document extends Eloquent
{
	// Database table used by object	
	public static $table = 'documents';
	
	public static $rules = array(
		'title' => 'required|min:2',
		'description' => 'required',
		'role' => 'required',
		/* Can only be a word document, PDF, image or excel spreadsheet (Filesize limit 10MB) */
		'image' => 'required|mimes:jpg,gif,png,pdf,doc,docx,xls|max:10240'
	);
	public static $rules2 = array(
		'title' => 'required|min:2',
		'description' => 'required',
		'role' => 'required',
		'assignto' => 'required'
	);
	
	public static $messages = array(
    	'image_required' => 'A file to upload is required',
    	'image_max' => 'Maximum upload file size is 10MB, The file you uploaded exceeds this size',
    	'image_mimes' => 'The uploaded file can only be a word document, PDF, image or excel spreadsheet'
	);
	
	/**
     * Add Document Validation Start Here
     *
	 * @param array $data Document data to validate
     * @return /Laravel/Validator
     */
	public static function validate($data)
	{
		return Validator::make($data, static::$rules, static::$messages);
	}

	/**
     * Edit Document Validation Start Here
     *
	 * @param array $data Document data to validate
     * @return /Laravel/Validator
     */
	public static function validate_new($data)
	{  
		return Validator::make($data, static::$rules2, static::$messages);
	}
	
	/**
     * Pagenavigation Start Here
     *
	 * @param int $per_page Records per page
     * @return object
     */
	public static function pagenavquery($per_page)
	{
		$orders = $users = DB::table('categories')
							->join('documents', 'documents.category_id', '=', 'categories.id')
							->paginate($per_page);
		return $orders->results;
	}
	
	/**
     * Pagenavigation Start Here
     *
	 * @param int $id Category ID
     * @return object
     */
	public static function select_cat($id)
	{
		$category = DB::table('categories')
						->join('documents', 'documents.category_id', '=', 'categories.id')
						->where('documents.id', '=', $id)
						->get(array('categories.categoryname', 'categories.id'));						
		return $category;
	}
	
	/**
     * Select author of a document
     *
	 * @param int $id Author (user) ID
     * @return object
     */
	public static function select_user($id)
	{
		$user = DB::table('users')
					->join('documents', 'documents.user_id', '=', 'users.id')
					->where('documents.id', '=', $id)
					->get(array('users.username', 'users.id'));						
		return $user;
	}

	/**
     * Returna comments tied to a document
     *
     * @return mixed
     */
	public function comments()
    {
    	return $this->has_many('Comment');
    }
} 
?>