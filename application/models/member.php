<?php
class Member extends Eloquent
{
	// Database table used by object
	public static $table = 'users';
	
	/**
     * Validation for editing Member
     *
	 * @param array $data Member data to validate
	 * @param int $id ID of member
     * @return object
     */
	public static function editvalidate($data, $id)
	{
		$members = DB::table('users')
	    				->where('email', '=', $data['email'])
	      				->where('id', '!=', $id)
          				->get(array('id', 'email'));					  
		return $members;
	}
	
	/**
     * Pagenavigation start here
     *
	 * @param int $id Member shown per page
     * @return object
     */
	public static function pagenavquery($per_page)
	{
		$orders = $users = DB::table('users')
							->paginate($per_page);
		return $orders->results;
	}
	
	/**
     * Random password generator (used for temporary authentication with changing password when forgotten)
     *
     * @return string
     */
	public static function randomPassword()
	{
		// Generates a random password
    	$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    	$pass = array();
    	$alphaLength = strlen($alphabet) - 1;
    	for ($i = 0; $i < 8; $i++) {
        	$n = rand(0, $alphaLength);
        	$pass[] = $alphabet[$n];
    	}
    	return implode($pass); // Turn the password random characters array into a string
	}

	/**
     * Validation to check if Member is permitted to view a category
     *
     * @param int $id ID of user
	 * @param int $id ID of category
     * @return boolean
     */
	public static function hasCatPerms($user_id, $cat_id)
	{
		$member = DB::table('categories_permissions')
	    			->where('user_id', '=', $user_id)
	      			->where('category_id', '=', $cat_id)
          			->get(array('user_id', 'category_id'));

        // If there is a result mark the member as permitted				  
		if (!empty($member)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
     * Function to add Member permissions for a category
     *
     * @param int $id ID of user
	 * @param int $id ID of category
     * @return void
     */
	public static function addCatPerms($user_id, $cat_id)
	{
		DB::table('categories_permissions')->insert(array(
			'user_id' => $user_id,
			'category_id' => $cat_id
		));
	}

	/**
     * Function to remove Member permissions for a category
     *
     * @param int $user_id ID of user
	 * @param int $cat_id ID of category, optionally leave blank to delete all permissions
     * @return void
     */
	public static function removeCatPerms($user_id, $cat_id = null)
	{
		$query = DB::table('categories_permissions')->where('user_id', '=', $user_id);

		// Delete category permission which matches user and category provided
		if (is_string($cat_id)) {
	        $query->where('category_id', '=', $cat_id)->delete();
		}
		// Deletes all permissions for user if not category ID provided
		else {
			$query->delete();
		}
	}
} 
?>