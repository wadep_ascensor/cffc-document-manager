<?php
class Comment extends Eloquent {
	/**
     * Creates a new comment and notifies anyone needing to know with an Email
     *
	 * @param int $data Data of comment just created/submitted
     * @return void
     */
	public static function submit($data)
	{
		// Notify relevant users, that the comment was created
		Mailer::commentPostedEmail($comment);		
	}
}
?>