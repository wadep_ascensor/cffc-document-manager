<?php
class Mailer
{
	/**
     * Send predefined email (using template for specific mailing purpose)
     *
	 * @param string $email_template Email template to use
	 * @param object $content_object Model used to extract information, on a per template basis
	 * @param object $user_objects Users objects relevant to the email
	 * @param array $to_from To and from email contact information
     * @return void
     */
	public function predefinedEmail($email_template, $content_object, $user_objects, $to_from){
		/* If there is an email content template available, use it */
		$templates_available = array(
			'commentposted', 'resetpassword'
		);

		if (in_array($email_template, $templates_available)) {
			// Get view with data
			$view = $this->htmlTemplate($email_template);
			
			/* Nest view and data */
			switch ($email_template) {
				case 'commentposted': // Tell a document author a comment has been posted
					// Extra data retrieval
					$document = Document::find($content_object->document_id);
					// Subject
					$subject = $this->subjectDefiner(array(
						'related_focus' => $user_objects['user']->username,
						'template_message' => "Posted a comment on your uploaded document",
						'main_focus' => $document->title
					));
					// Nest heading and content
					$view->nest($email_template, "mail.templates._".$email_template, array( // Send data to the partial
						'username' => $user_objects['user']->username,
						'document_title' => $document->title,
						'comment' => $content_object->comment
					));
					break;
			
				case 'resetpassword': // Send password reset verification email
					// Subject
					$subject = $this->subjectDefiner(array(
						'template_message' => "New password request for user",
						'main_focus' => $user_objects['user']->username
					));
					// Nest heading and content
					$view->nest($email_template, "mail.templates._".$email_template, array( // Send data to the partial
						'username' => $user_objects['user']->username,
						'reset_link' => $content_object
					));
					break;
			}
		}
		else {
			die("Email template does not exist");
		}
		
		$contacts = $this->setEmailContacts($user_objects, $to_from); // Set sender and recipitent
		$this->sendEmail($contacts, $subject, $view); // Send the email
	}

	/**
     * Setup an array containing contact information for sender and recipitent of an email
     *
	 * @param object $user_objects Users objects relevant to the email
	 * @param array $to_from To and from email contact information
     * @return array
     */
	public function setEmailContacts($user_objects, $users){ /* Set email, with name of user shown as contact */
		$contacts = array(
			'to' => array($user_objects[current($users)]->email => $user_objects[current($users)]->username),
			'from' => array($user_objects[end($users)]['email'] => $user_objects[end($users)]['email'])
		);
		return $contacts; // Return contacts (to and from) array
	}
	
	/**
     * Produce a valid subject title, using an array with a formula for information abstraction
     *
	 * @param array $subject_array Contains all subject information
     * @return string
     */
	public function subjectDefiner($subject_array){
		if(!isset($subject_array['related_focus'])){
			$subject_array['related_focus'] = ""; // Set to whitespace if not passed
		}
		$subject_array['main_focus'] = "'".$subject_array['main_focus']."'"; // Highlight main focus
		
		return(implode(" ", $subject_array)); // Return subject as string
	}
	
	/**
     * Create view object (layout) used for an email being sent
     *
	 * @param string $email_template Email template to embed in the layout
     * @return /Laravel/View
     */
	public function htmlTemplate($email_template){ /* Produce full page email HTML */
		$view = View::make('mail.content') // Mail view template used for  HTML of email (views/mail/content.php)
					->with('template_used', $email_template)
					->nest($email_template, 'mail.templates._'.$email_template); // Bind the partial
		return($view);
	}
	
	/**
     * Send an email
     *
	 * @param array $contacts Email address and contact information, of both recipitent and sender of email
	 * @param string $subject String containing email subject line
	 * @param object $html Laravel View (Layout and nested views)
     * @return void
     */
	public function sendEmail($contacts, $subject, $html){
		// Email server settings
		$transporter = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        				->setUsername('ascensor.dev@gmail.com')->setPassword('ascensor9871');
 		$mailer = Swift_Mailer::newInstance($transporter);
		
		// Email setup
		$message = Swift_Message::newInstance($subject)
  					->setFrom($contacts['from'])
  					->setTo($contacts['to'])
  					->setBody('','text/plain')
    				->addPart($html,'text/html');

		// Send to recipitent
		$mailer->send($message);
	}
} 
?>