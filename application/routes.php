<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function()
{
	return View::make('user.login');
});

// ALL PAGES THAT REQUIRE USER TO BE LOGGED IN (REDIRECTS TO LOGIN PAGE IF NOT)

/**
 * Home code start here
 *
 * @return void
 */
/* Anyone can access this page */
Route::any('homepage', array('before' => 'sentry', function(){
}, 'uses' => 'homepage@index'));

/**
 * Categories code start here
 *
 * @return void
 */
/* Only admins can access these pages */
Route::get('category', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'category@index'));
Route::get('category/new', array('before' => 'sentry|only_admin', function(){
}, 'as' => 'new_category','uses' => 'category@new'));
Route::post('category/create', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'category@create'));
Route::get('category/(:any)/edit', array('before' => 'sentry|only_admin', function(){
}, 'as' => 'edit_category','uses' => 'category@edit'));
Route::put('category/update', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'category@update'));
Route::get('category/(:any)/delete', array('before' => 'sentry|only_admin', function(){
}, 'as' => 'delete_category','uses' => 'category@delete'));

/**
 * Documents code start here
 *
 * @return void
 */
/* Anyone can access this page */
Route::any('document', array('before' => 'sentry', function(){
}, 'uses' => 'document@index'));
Route::any('document/view/(:any)', array('before' => 'sentry', function(){
}, 'uses' => 'document@view'));
/* Only authors and admins can access these pages */
Route::any('document/new', array('before' => 'sentry|user_restriction', function(){
}, 'as' => 'new_document','uses' => 'document@new'));
Route::any('document/create', array('before' => 'sentry|user_restriction', function(){
}, 'uses' => 'document@create'));
Route::any('document/edit/(:any)', array('before' => 'sentry|user_restriction', function(){
}, 'as' => 'edit_document','uses' => 'document@edit'));
Route::any('document/update', array('before' => 'sentry|user_restriction', function(){
}, 'uses' => 'document@update'));
Route::any('document/delete/(:any)', array('before' => 'sentry|user_restriction', function(){
}, 'as' => 'delete_document','uses' => 'document@delete'));
	
/**
 * Users code start here
 *
 * @return void
 */
/* Anyone can access this page */
Route::get('user/logout', array('before' => 'sentry', function(){
}, 'uses' => 'user@logout'));
/* Users or Authors can access this page for there own account only, or if admin */
Route::any('user/edit/(:any)', array('before' => 'sentry|owner_or_admin', function(){
}, 'as' => 'edit_user','uses' => 'user@edit'));
/* Only admins can access these pages */
Route::any('user/create', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'user@create'));
Route::any('user/register', array('before' => 'sentry|only_admin', function(){
}, 'as' => 'new_user', 'uses' => 'user@register'));
Route::get('user/delete/(:any)', array('before' => 'sentry|only_admin', function(){
}, 'as' => 'delete_user', 'uses' => 'user@delete'));
	
/**
 * Members code start here
 *
 * @return void
 */
/* Only admins can access these pages */
Route::any('member/account', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'member@account'));
Route::any('member/list', array('before' => 'sentry|only_admin', function(){
}, 'uses' => 'member@list'));

// ANY PAGES THAT ANYONE CAN VIEW (Regardless of been logged in or permissions)
Route::get('user/activate/(:any)/(:any)', 'user@activate');
Route::any('user/login', 'user@login');
Route::any('user/reset', 'user@reset');
Route::get('user/confirmation/(:any)/(:any)', 'user@confirmation');
Route::post('user/confirmation', 'user@confirmation');

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
*/
Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
*/
Route::filter('sentry', function()
{
	
    // Check if the user is logged in.
    if (!Sentry::check()) {
        // Store the current uri in the session.
        Session::put('login_redirect', URI::current());
        // Redirect to the login page.
        return Redirect::to('user/login');
    }

	// Register form macro
	Form::macro('comment_delete_button', function($comment_id)
	{
    	return '<button data-dismiss="alert" class="close delete-comment" value="'.$comment_id.'">×</button>';
	});
	
	// Load Swiftmailer
	Bundle::start('swiftmailer');
});

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});
Route::filter('only_admin', function(){
	// Permissions filter for users in "Admin" group
	$user = Session::get(Config::get('sentry::sentry.session.user'));
	$users = Sentry::user($user);
	$groupid_login = $users->group_id;
	if (Sentry::group($groupid_login)->get('name') != 'Admin') {
		return Redirect::to_secure('document')
					->with('message','You do not have permission to access that page')
					->with('mode','danger');
	}
});
Route::filter('user_restriction', function(){
	// Permissions filter for users in "Author" group
	$user = Session::get(Config::get('sentry::sentry.session.user'));
	$users = Sentry::user($user);
	$groupid_login = $users->group_id;
	if (Sentry::group($groupid_login)->get('name') != 'Author' && Sentry::group($groupid_login)->get('name') != 'Admin') {
		return Redirect::to_secure('document')
					->with('message','You do not have permission to access that page')
					->with('mode','danger');
	}
});
Route::filter('owner_or_admin', function(){
	// Permissions filter for owners (users controller only) or admins
	$user = Session::get(Config::get('sentry::sentry.session.user'));
	$users = Sentry::user($user);
	$groupid_login = $users->group_id;
	if (Sentry::group($groupid_login)->get('name') != 'Admin' && Sentry::user()->id != URI::segment(3)) {
		return Redirect::to_secure('document')
					->with('message','You do not have permission to access that page')
					->with('mode','danger');
	}
});