<?php

return array(
	// Number of characters to limit be shown in the popover, when viewing a document name
	'description_popover_length' => 250,

	// Number of characters to limit the document name by
	'document_name_length' => 35
);