<?php
class Createadmin_Task {
    public function run($arguments)
    {
    	// Depedencies to setup
    	Session::load();
    	Bundle::start('sentry');
		
    	// Create the user
    	$admin_user_id = 1;
    	$admin_user = Sentry::user()->register(array(
    		'id' => $admin_user_id,
    		'email'    => 'john.doe@example.com',
        	'password' => 'mypass',
        	'username' => 'admin',
        	'group_id' => 1, // GROUPING (sentry none standard)
			'status' => 1
		));
		   
		if(!$admin_user){
			// something went wrong - shouldn't really happen
        	die("Could not create admin");
   		}
    	else{
        	// the user was created
        	die("Admin created sucessfully");
    	}
    }
}
?>