<?php

class Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// Raw SQL from dump for creation
		DB::query('CREATE TABLE `categories` (
  		`id` int(100) NOT NULL AUTO_INCREMENT,
  		`categoryname` varchar(250) NOT NULL,
  		`updated_at` datetime NOT NULL,
  		`created_at` datetime NOT NULL,
  		PRIMARY KEY (`id`)
		) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;');
		
		// Insert sample category so application doesn't break when adding document on first use
		DB::query("INSERT INTO `categories`"
		." VALUES ('1', 'Sample cateogory', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."');");
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		// Raw SQL from dump for deletion
		DB::query('DROP TABLE IF EXISTS `categories`;');
	}

}