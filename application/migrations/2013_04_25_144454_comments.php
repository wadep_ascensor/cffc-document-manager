<?php

class Comments {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// Raw SQL from dump for creation
		DB::query('CREATE TABLE `comments` (
  		`id` int(64) NOT NULL AUTO_INCREMENT,
  		`user_id` int(64) NOT NULL,
 		`document_id` int(64) NOT NULL,
  		`comment` longtext NOT NULL,
  		`updated_at` datetime NOT NULL,
  		`created_at` datetime NOT NULL,
  		PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		// Raw SQL from dump for deletion
		DB::query('DROP TABLE IF EXISTS `comments`;');
	}

}