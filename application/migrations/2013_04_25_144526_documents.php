<?php

class Documents {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// Raw SQL from dump for creation
		DB::query('CREATE TABLE `documents` (
  		`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  		`title` varchar(200) NOT NULL,
  		`description` text NOT NULL,
  		`user_id` int(10) unsigned NOT NULL,
  		`category_id` varchar(200) NOT NULL,
  		`image` varchar(300) NOT NULL,
  		`created_at` datetime NOT NULL,
 		`updated_at` datetime NOT NULL,
  		PRIMARY KEY (`id`),
  		KEY `user` (`user_id`) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		// Raw SQL from dump for deletion
		DB::query('DROP TABLE IF EXISTS `documents`;');
	}

}