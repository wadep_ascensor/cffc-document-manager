<?php

class Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// THIS IS A TABLE MODIFICATION CODE
		/* Original developers use a noe standard way of storing the group ID in Sentry, so we have to create
		the none standard column, after sentry migrate has run */
		DB::query('ALTER TABLE users ADD group_id int(11) NOT NULL AFTER updated_at;');
		
		// Create the groups specific to this application
		DB::query("INSERT INTO `groups` VALUES ('1', 'Admin', 'Administrative Privileges'),
		('2', 'User', 'Access to Everything'),
		('3', 'Author', 'only certain fields');");
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}