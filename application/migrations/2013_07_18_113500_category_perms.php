<?php

class Category_Perms {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		// create table
		Schema::table('categories_permissions', function($table)
		{
    		$table->create();
        	$table->integer('user_id');
        	$table->integer('category_id');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop table
		Schema::drop('categories_permissions');
	}

}