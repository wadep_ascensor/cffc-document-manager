<?php

class Category_Controller extends Base_Controller {
	/**
	 * Controller implements restful interface
	 * 
	 * @var bool
	 */
	public $restful = true;
	
	/**
	 * Show List of Categories
	 * 
	 * @param void
	 * @return \Laravel\View
	 */
	public function get_index()
	{
		// Get user perms
	    $user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		
		// Pagination variables
		$rec = Category::all();
		$per_page = 20;
		
		// Build and return view
		$orders = Paginator::make($rec, count($rec), $per_page);
		$view = View::make('category.index')
					->with('context', Category::$context)
					->with('orders', $orders)
					->with('groupid_login', $users->group_id)
					->with('categories' , Category::pagenavquery($per_page, array(), false));

		return $view;
	}
	
	/**
     * Show New Category form
     *
     * @return mixed
     */
	public function get_new()
	{
		// Get user perms (redirect if not valid)
	    $user = Session::get(Config::get('sentry::sentry.session.user'));
	    $user_group = Sentry::user($user);
		
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
			return Redirect::to_secure('category')
					->with('message', 'You dont have permissions to access the adding category');
		}
		
		$userid = Session::get('id');
		
		// Build and return view
		return view::make('category.new')
				   ->with('context', Category::$context)
				   ->with('title', 'Add New '.Category::$context)
				   ->with('users', Member::all());
	}
	
	/**
     * Show / Edit Category form
     *
	 * @param int $id Category ID
     * @return mixed
     */
	public function get_edit($id)
	{
		// Get user perms
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$user_group = Sentry::user($user);
		
		// Check if category exists
		if (Category::find($id) != null) {
			
			if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
				return Redirect::to_secure('category')
						->with('message', 'You dont have permissions to access the edit category');
			}
			
			// Build and return view
			return View::make('category.edit')
					   ->with('context', Category::$context)
					   ->with('title','Edit Category details')
					   ->with('category',Category::find($id))
					   ->with('users', Member::all());
		}
		else {
			return Redirect::to_secure('category')
					->with('message', Category::$context.' does not exist')
					->with('mode', 'danger');	
		}		 
	}

	/**
     * Adding New Category
     *
	 * @return \Laravel\Redirect
     */	
	public function post_create()
	{
		// Get user perms (redirect if not valid)
	    $user = Session::get(Config::get('sentry::sentry.session.user'));
		$user_group = Sentry::user($user);
		
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
			return Redirect::to_secure('category')
					->with('message', 'You dont have permissions to access the adding members');
		}

		// Build validation object
		$validation = Category::validate(Input::all());
		
		/* Validation failed */
		if ($validation->fails()) {
			return Redirect::to_secure('category/new')
					->with_errors($validation)
					->with_input();
		} 
		/* Validation successful */
		else { 

			// Create CATEGORY
			$category = Category::create(array(
				'categoryname'=>Input::get('categoryname')			
			));

			// Get ID from newly saved category
			$id = $category->id;

			/* Process initial permissions for users to view */
			foreach(Member::all() as $member) {

				$viewing_permitted = (int)Input::get('User'.($member->id), '0');
				
				// Allow the member to be permitted to view current category (if not already)
				if ((boolean)$viewing_permitted) {
					if (!Member::hasCatPerms($member->id, $id)) {
						Member::addCatPerms($member->id, $id);
					}
				}
			}
			
			// Redirect to CATEGORY LIST
			return Redirect::to_secure('category')
					->with('message', 'The '.Category::$context.' created successfully')
					->with('mode', 'success');
		}
	} 

	/**
     * Update Category
     *
	 * @param void
     * @return \Laravel\Redirect
     */
	public function put_update()
	{
		// Get user perms (redirect if not valid)
	    $user = Session::get(Config::get('sentry::sentry.session.user'));
	    $user_group = Sentry::user($user);
		
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
			return Redirect::to_secure('member/list')
					->with('message','You dont have permissions to delete the members');
		}

		// Build variables
	    $id = Input::get('id');
		$name = Input::get('categoryname');		
		$checkcategory = Category::editvalidate(Input::all());
		$validation = Category::validate1(Input::all());
		
		/* Category exists */
		if (count($checkcategory) == 1){
			// Redirect to CATEGORY EDIT
			return Redirect::to_route('edit_category',$id)
					->with('message', 'The '.Category::$context.' Name Already Taken');
		}
		/* Validation failed */
		elseif ($validation->fails()) {
			// Redirect to CATEGORY EDIT
			return Redirect::to_route('edit_category',$id)
					->with_errors($validation)
					->with_input();
		}
		/* Validation successful */
		else {

			/* Process changes in permissions for users to view */
			foreach(Member::all() as $member) {

				$viewing_permitted = (int)Input::get('User'.($member->id), '0');

				// Allow the member to be permitted to view current category (if not already)
				if ((boolean)$viewing_permitted) {
					if (!Member::hasCatPerms($member->id, $id)) {
						Member::addCatPerms($member->id, $id);
					}
				}
				// Remove permissions for anyone whose permissions have been revoked
				elseif(Member::hasCatPerms($member->id, $id)) {
					Member::removeCatPerms($member->id, $id);
				}
			}

			// Update CATEGORY
			Category::update($id,array('categoryname'=>Input::get('categoryname')));
			// Redirect to CATEGORY LIST
			return Redirect::to_secure('category')
					->with('message', 'The '.Category::$context.' Updated successfully')
					->with('mode', 'success');
		}
	}
	
	/**
     * Delete Category
     *
	 * @param int $id Category ID
     * @return \Laravel\Redirect
    */	
	public function get_delete($id)
	{
		// Get user perms
		$user = Session::get(Config::get('sentry::sentry.session.user'));
	    $user_group = Sentry::user($user);
		
		/* Check if category exists */
		if (Category::find($id) != null) {

			/* Permissions valid */
			if (Sentry::group($user_group->group_id)->get('name')!='Admin') {
				return Redirect::to_secure('member/list')
						->with('message','You dont have permissions to delete the members');
			}

			// Delete category permissions
			DB::table('categories_permissions')->where('category_id', '=', $id)->delete();
			
			// Delete CATEGORY
			DB::table('categories')->delete($id);
			
			return Redirect::to_secure('category')
					->with('message', 'The '.Category::$context.' deleted successfully')
					->with('mode', 'success');
		} 
		/* Category already deleted */
		else {
			return Redirect::to_secure('category')
					->with('message', Category::$context.' already deleted')
					->with('mode', 'danger');
		}
	}
}