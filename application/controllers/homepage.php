<?php

class Homepage_Controller extends Base_Controller {
	/**
	 * Controller implements restful interface
	 * 
	 * @var bool
	 */
	public $restful = true;
	
	/**
	 * Show List of Categories
	 * 
	 * @param void
	 * @return \Laravel\View
	 */
	public function get_index()
	{
		// Get user perms
	    $user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		
		// Pagination variables
		$rec = Category::all();
		$per_page = 20;

		// Get all categories IDs which the user has access to view
		$permitted_categories = DB::table('categories_permissions')
	      						  ->where('user_id', '=', $user)
         						  ->lists('category_id');
		
		// Build and return view
		$orders = Paginator::make($rec, count($rec), $per_page);
		$view = View::make('homepage.index')
					->with('orders', $orders)
					->with('groupid_login', $users->group_id)
					->with('categories' , Category::pagenavquery($per_page, $permitted_categories));

		return $view;
	}
}