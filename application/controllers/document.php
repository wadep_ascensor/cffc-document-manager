<?php

class Document_Controller extends Base_Controller {

	/**
     * List of documents
     *
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_index()
	{
		// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		
		if (Input::has('page')) { $page_num = Input::get('page'); }
		else { $page_num = 1; } /* Page number (for pagination) */
		
		// Pagination overview logic formulas
		$records_per_page = 20; /* Manually change this to desired LIMIT */
		$pagination_details = array(
			'record_start_num' => ($page_num - 1) * $records_per_page,
			'record_end_num' => $page_num * $records_per_page
		);
		
		// Filter the results and indicate a search filter is on (if applicable)
		if (Input::has('search_query')) { $search_query = Input::get('search_query'); }
		else { $search_query = ''; }
		
		// Integrated category logic
		if (Input::has('catfilter')) {
			Session::put('catfilter', Input::get('catfilter'));
		}

		/* Category record filter */
		if (Session::has('catfilter') && Session::get('catfilter') != 0 && is_object(Category::find(Session::get('catfilter')))) {
			$cat_selected = Session::get('catfilter');
			
			// Document
			$documents = Document::take($records_per_page)
							->skip($pagination_details['record_start_num'])
							->where('category_id', '=', $cat_selected)
							->where(function($query) use($search_query)
    						{
    							/* Search queries */
       							$query->where('title', 'LIKE', '%'.$search_query.'%');
        						$query->or_where('description', 'LIKE', '%'.$search_query.'%');
								$query->or_where('username', 'LIKE', '%'.$search_query.'%');
    						})
							->left_join('users', 'users.id', '=', 'documents.user_id')
							->order_by('documents.updated_at', 'desc')
							->get(array('documents.*', 'users.username'));
				
			// Counter
			$pagination_details['records_count'] = DB::table('documents')
													->where('category_id', '=', $cat_selected)
													->where(function($query) use($search_query)
    												{
    													/* Search queries */
       													$query->where('title', 'LIKE', '%'.$search_query.'%');
        												$query->or_where('description', 'LIKE', '%'.$search_query.'%');
														$query->or_where('username', 'LIKE', '%'.$search_query.'%');
    												})
													->left_join('users', 'users.id', '=', 'documents.user_id')
													->order_by('documents.updated_at', 'desc')
    												->count();
		}
		// Default record filter
		else {
			$cat_selected = 0;
			$documents = array();
			$pagination_details['records_count'] = 0;
		}
		
		// Final resultset trim
		if ($pagination_details['record_end_num'] > $pagination_details['records_count']) {
			$pagination_details['record_end_num'] = $pagination_details['records_count'];
		}
		$pagination_details['record_start_num']++;
		
		// Categories 
		$data = DB::table('categories')
				->get(array('id','categoryname'));
		$categories[0] = "Select Category";
		foreach ($data as $key => $value) {
            $categories[$value->id] = $value->categoryname; // Create the options array
        }
		
		/* Pagination UI generator */
		$orders = Paginator::make($documents, $pagination_details['records_count'], $records_per_page);
		/* URL GET variables generator */
		if ($search_query != '') {
       		$get_variables = array('search_query' => $search_query, 'catfilter' => $cat_selected);
		}
		else {
			$get_variables = array('catfilter' => $cat_selected);
		}
		
		// View
	    $view = View::make('document.index')
	   				->with('orders', $orders) // Pagination navigation
	        		->with('documents', $documents) // Records
	        		->with('categories', $categories) // Category selection
	        		->with('cat_selected', $cat_selected) // Category selected
					->with('search_query', $search_query) // Search query
					->with('pag_details', $pagination_details) // Pagination overview
					->with('groupid_login', $users->group_id) // User permissions
					->with('get_variables', $get_variables); // Variables used in URL GET method
	    return $view;
	}

	/**
     * To Display New Document Form
     *
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_new()
	{
		$userid = Session::get('id');
		$user = Session::get(Config::get('sentry::sentry.session.user'));

		// Get all categories IDs which the user has access to view
		$permitted_categories = DB::table('categories_permissions')
	      						  ->where('user_id', '=', $user)
         						  ->lists('category_id');

        $query = DB::table('categories');

		// Apply permissions if passed
		if (!empty($permitted_categories)) {
			$data = $query->where_in('id', $permitted_categories)
						  ->get(array('id','categoryname'));
		}
		// No permissions, dont get any categories
		else {
			$data = array();
		}

		foreach ($data as $key => $value) {
            $categories[$value->id] = $value->categoryname; // Create the options array
        }

        // If no categories setup, show a blanked option
		if(!isset($categories)){
			$categories = array('0' => "No categories available");
		}

		return $view = view::make('document.new')
						->with('title', 'Add New Document')
						->with('categories', $categories);			
	}
	
	/**
     * Adding New Document
     * 
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_create()
	{
		// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		$groupid_login = $users->group_id;
		
		$validation= Document::validate(Input::all());
		if ($validation->fails()) {
			return Redirect::to_secure('document/new')
					->with_errors($validation)->with_input();
		}
		else {
			/* Upload file with random hash */
			$var = Input::file('image.name');
			$rand = rand();
			$image = $rand . '_' . Input::file('image.name');
			
			Input::upload('image', 'public/uploads', $rand . '_' . $var); // Save image to server
			
			/* Create document */
			Document::create(array(
				'title' => Input::get('title'),
				'image' => $image,
				'user_id' => Sentry::user()->id,
				'description' => Input::get('description'),
				'category_id' => Input::get('role')
			));
			
			/* Return to list of documents */
			return Redirect::to_secure('document')
					->with('message', 'Document added successfully')
					->with('mode', 'success');
		}
	}

	/**
     * To Display Edit Document form
     *
	 * @param string $id Document ID
     * @return mixed 
     */
	public function action_edit($id)
	{
		// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		$groupid_login = $users->group_id;
		
		if (Document::find($id) != null) {
			if (Sentry::user()->id == Document::find($id)->user_id || Sentry::group($groupid_login)->get('name') == 'Admin') {

				// Get all categories IDs which the user has access to view (with relevant record selected)
				$permitted_categories = DB::table('categories_permissions')
	      						  		  ->where('user_id', '=', $user)
         						  		  ->lists('category_id');

        		$query = DB::table('categories');

				// Apply permissions if passed
				if (!empty($permitted_categories)) {
					$data = $query->where_in('id', $permitted_categories)
						 		  ->get(array('id','categoryname'));
				}
				else {
					$data = array();
				}

				foreach ($data as $key => $value) {
	            	$categories[$value->id] = $value->categoryname; // Create the options array
	        	}

	        	// If no categories setup, show a blanked option
				if(!isset($categories)){
					$categories = array('0' => "No categories available");
				}
				$selected_category = Document::select_cat($id);
				
				if(empty($selected_category)){
					if(empty($categories)){
						// No categories in use
						$cat = "No categories available";
						$categories = array('0' => "No categories available");
					}
					else{
						$cat = "";
					}
					
				}
				else{
					$cat = $selected_category[0]->categoryname;
				}
			
				// Get users (with relevant record selected)
		    	$data = DB::table('users')
		    				->get(array('id','username'));
				foreach ($data as $key => $value) {
	           		$all_users[$value->id] = $value->username; // Create the options array
	        	}
				$selected_user = Document::select_user($id);
				if (isset($selected_user[0])) {
					$use = $selected_user[0]->username;
					$user_blocked_choice = "";
				}
				else {
					$all_users[-1] = "Not assigned to anyone (Pick a user to assign to)";
					$use = $all_users[-1];
					$user_blocked_choice = -1;
				}
				
			
		    	return View::make('document.edit')
						->with('title', 'Edit Document details')
						->with('users', $all_users)
						->with('select_users', $use)
						->with('user_blocked_choice', $user_blocked_choice)
						->with('categories', $categories)
						->with('select_categories', $cat)
						->with('document', Document::find($id));
			}
			else {
				// Doesn't have permission to edit this document
				return Redirect::to_secure('document')
							->with('message', 'You do not have permission to edit this file')
							->with('mode', 'danger');
			}
		}
		else {
			// Document doesn't exist
			return Redirect::to_secure('document')
						->with('message', 'Document does not exist')
						->with('mode', 'danger');
		}	 
	}

	/**
     * Update Document
     * 
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_update()
	{
	 	// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		$groupid_login = $users->group_id;

	    $id = Input::get('id');
		$image_hidden = Input::get('hid_image'); 
		$img = Input::file('image.name');
		if ($img != '') {
			if (Sentry::user()->id == Document::find($id)->user_id || Sentry::group($groupid_login)->get('name') == 'Admin') {
				// Delete file tied to document
				File::delete('public/uploads/' . $image_hidden);
			
				// Upload new document to use
				$var = Input::file('image.name');
				$rand = rand();
				$image = $rand . '_' . Input::file('image.name');	
				Input::upload('image', 'public/uploads' , $rand . '_' . $var);	
			}
		}
		
		$validation = Document::validate_new(Input::all());
        if ($validation->fails()) {
			return Redirect::to_route('edit_document',$id)
						->with_errors($validation)
						->with_input();
		}
		elseif (Sentry::user()->id == Document::find($id)->user_id || Sentry::group($groupid_login)->get('name') == 'Admin') {
			// Overwrite document data if user is owner or admin (fail silently if not)	
			if ($img !='') {
				Document::update($id, array(
					'title' => Input::get('title'),
					'image' => $image,
					'description' => Input::get('description'),
					'category_id' => Input::get('role'),
					'user_id' => Input::get('assignto')	
				));
			}
			else {
				Document::update($id, array(
					'title' => Input::get('title'),
					'description' => Input::get('description'),
					'category_id' => Input::get('role'),
					'user_id' => Input::get('assignto')
				));	
			}					
			return Redirect::to_secure('document')
						->with('message','Document edited successfully')
						->with('mode','success');
		  }
	 }
	 
	 /**
     * Delete Document
     *
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_delete($id)
	{
		// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		$groupid_login = $users->group_id;
		
		if (Document::find($id) != null){
			if (Sentry::user()->id == Document::find($id)->user_id || Sentry::group($groupid_login)->get('name') == 'Admin') {
				/* Delete document and resources tied to document */
				File::delete('public/uploads/' . Document::find($id)->image); // Delete file tied to document
				DB::table('documents')
					->delete($id); // Delete document record
				DB::table('comments')
					->where('document_id', '=', $id)
					->delete(); // Delete comments tied to document
		
				// Go to document list, and show indicative message of previous action
				return Redirect::to_secure('document')
							->with('message', 'Document deleted')
							->with('mode', 'danger');
			}
			else {
				// Doesn't have permission to edit this document
				return Redirect::to_secure('document')
							->with('message', 'You do not have permission to delete this file')
							->with('mode', 'danger');
			}
		} else{
			// Go to document list, and show indicative message of previous action even though record already deleted
			return Redirect::to_secure('document')
						->with('message', 'Document already deleted')
						->with('mode', 'danger');
		}
	}

	 /**
     * View Document
     *
	 * @param string $id Document ID
     * @return mixed
     */
	public function action_view($id){
		// Grab users groups permissions
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		$groupid_login = $users->group_id;
		
		// Models
		$document = Document::find($id);
		if (Sentry::user()->id == $document->user_id && Sentry::group($groupid_login)->get('name') == 'Author') {
			$has_permission = true;
			$is_admin = false;
		}
		elseif (Sentry::group($groupid_login)->get('name') == 'Admin') {
			$has_permission = true;
			$is_admin = true;
		}
		else {
			$has_permission = false;
			$is_admin = false;
		}
		
		/* User has submitted a comment */
		if (Input::has('comment-text')) {
			// Store comment in DB
			$creation_date = date('Y-m-d H:i:s');
			Comment::create(array(
				'user_id' => Sentry::user()->id,
				'document_id' => $id,
				'comment' => Input::get('comment-text'),
				'updated_at' => $creation_date,
				'created_at' => $creation_date
			));
			
			$author_id = Document::find($id)->user_id;
			
			if (Sentry::user_exists(intval($author_id))) {
				// Send an email to author, notifying of new comment if author user exists (not deleted)
				$email = new Mailer();
				$content_object = DB::table('comments')
				->where('user_id', '=', Sentry::user()->id)
				->where('document_id', '=', $id)
				->where('updated_at', '=', $creation_date)
				->where('created_at', '=', $creation_date)
				->get();
				
				// Set user objects (used in email)
				$site_domain = preg_replace('#^https?://#', '', URL::base());
				$user_objects = array(
					'user' => Member::find(Sentry::user()->id),
					'system' => array('email' => "noreply@".$site_domain), // Statically coded, as generic noreply address
					'author' => Member::find($author_id)
				);
			
				// Send email
				$email->predefinedEmail('commentposted', $content_object[0], $user_objects, array(
					'author', 'system'
				));
			}
		}
		/* User has deleted a comment */
		if (Input::has('delete-comment')) {
			$comment_to_delete = Comment::find(Input::get('delete-comment'));
			// Delete comment if user is owner or admin (fail silently if not)
			if (Sentry::user()->id == $comment_to_delete->user_id || Sentry::group($groupid_login)->get('name') == 'Admin') {
				DB::table('comments')
					->delete($comment_to_delete->id);
			}
		}
		
		// Page number
		if (Input::has('page')) { $page_num = Input::get('page'); }
		else { $page_num = 1; }	
		
		// Pagination overview logic formulas
		$records_per_page = 20; /* Manually change this to desired LIMIT */
		$pagination_details = array(
			'record_start_num' => ($page_num - 1) * $records_per_page,
			'record_end_num' => $page_num * $records_per_page
		);
		
		$comments = Comment::take($records_per_page)->skip($pagination_details['record_start_num'])
						->where('document_id', '=', $id)
						->left_join('users', 'users.id', '=', 'comments.user_id')
						->get(array('comments.*', 'users.username'));
						
		// Pagination record count
		$pagination_details['records_count'] = DB::table('comments')
												->where('document_id', '=', $id)->count();

		// Final resultset trim
		if ($pagination_details['record_end_num'] > $pagination_details['records_count']) {
			$pagination_details['record_end_num'] = $pagination_details['records_count'];
		}
		$pagination_details['record_start_num']++;
		
		/* Pagination UI generator */
		$orders = Paginator::make($comments, $pagination_details['records_count'], $records_per_page);
		
		// Models
		$document = Document::find($id);
		$userid = Session::get('id');
		
		// Data + View
		return $view = view::make('document.view')
						->with('document',$document) // Document itself
						->with('comments',$comments) // Comments linked to the document
						->with('orders',$orders) // Pagination navigation
						->with('pag_details',$pagination_details) // Pagination overview
						->with('has_permission',$has_permission) // User permissions
						->with('is_admin',$is_admin); // Admin override
	}
}