<?php

class User_Controller extends Base_Controller {
	/**
	 * Controller implements restful interface
	 * 
	 * @var bool
	 */
    public $restful = true;

    /**
     * Show register form
     *
     * @return mixed
     */
    public function get_register()
    {
    	// Get perms
		$user = Session::get(Config::get('sentry::sentry.session.user'));
	    $user_group = Sentry::user($user);
		
		/* Permissions valid */
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
			return Redirect::to_secure('member/list')
					->with('message', 'You dont have permissions to access the adding members page')
					->with('mode', 'danger');
		}
		
		// Build and return view
		return View::make('user.registration')
                   ->with('categories', Category::all());
	}

    /**
     * Do register
     *
     * @return \Laravel\Redirect
     */
    public function post_register()
    {
    	// Get perms
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$user_group = Sentry::user($user);
		
		/* Permissions valid */
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin') {
			return Redirect::to_secure('member/list')
					->with('message','You dont have permissions to access the adding members page')
					->with('mode','danger');
		}

        // data pass to the view
        $data = array();

		// do validation
        $rules = array(
			'name' => 'required',
            'email' => 'unique:users|required|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::to('user/register/')
            		->with_input()
            		->with_errors($validation);
        }

        // add user
        try {
        	// Get checkbox value
			$checkbox = Input::get('inlineCheckbox1');
			/* Default value */
			if ($checkbox == "") {
				$status = '1';
			}
			/* Value not default */
			else {
				$status = '0';
			}

			// Create user
            $user = Sentry::user()->register(array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
				'username' => Input::get('name'),
				'group_id' => Input::get('role'),
				'status' => $status
            ));
		   
		   /* Failed to create user */
            if (!$user) {
                $data['errors'] = 'There was an issue when add user to database';
            }
        }
        catch (Sentry\SentryException $e){
            $data['errors'] = $e->getMessage();
        }
		
		/* Errors exist */
        if (array_key_exists('errors', $data)) {
            return Redirect::to('user/register/')
            		->with_input()
            		->with('errors', $data['errors']);
        }
		/* Errors don't exist */
        else {

            /* Process initial permissions for users to view */
            foreach(Category::all() as $category) {

                $viewing_permitted = (int)Input::get('Category'.($category->id), '0');

                // Allow the member to be permitted to view current category (if not already)
                if ((boolean)$viewing_permitted) {
                    if (!Member::hasCatPerms($user['id'], $category->id)) {
                        Member::addCatPerms($user['id'], $category->id);
                    }
                }
            }

            return Redirect::to('member/list')
            		->with('hash_link', URL::base() . 'user/activate/' . $user['hash'])
					->with('message', 'Member added succesfully.')
					->with('mode', 'success');
        }
    }

	/**
     * Activate user
     *
	 * @param string $email Email Address (Base64 encrypted)
	 * @param string $hash Secret phrase (Base64 encrypted)
     * @return mixed
     */
    public function get_activate($email = null, $hash = null)
    {
        try {
        	// Try activation code
            $activate_user = Sentry::activate_user($email, $hash);

			/* Activation code valid */
            if ($activate_user) {
                return Redirect::to('user/login');
            }
			/* Activation code not valid */
            else {
                echo 'The user was not activated';
            }
        }
        catch (Sentry\SentryException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Show login form
     *
     * @return mixed
     */
    public function get_login()
    {
    	/* User not logged in */
		if (!Sentry::check()) {
        	return View::make('user.login');
		}
		/* User logged in */
		else {
			return Redirect::to_secure('document');
		}
    }

    /**
     * Do login
     *
     * @return \Laravel\Redirect
     */
    public function post_login()
    {
        // do valiation
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $input = Input::get();
        $validation = Validator::make($input, $rules);

		/* Validation failed */
        if ($validation->fails()) {
            return Redirect::to('user/login')
            		->with_input()
            		->with_errors($validation);
        }

        try {
        	// Validate login
            $valid_login = Sentry::login(Input::get('email'), Input::get('password'), Input::get('remember'));

			/* User logged in */
            if ($valid_login) {
                return Redirect::to('homepage');
            }
			/* User not logged in */
            else {
                $data['errors'] = "Invalid login!";
            }
        }
        catch (Sentry\SentryException $e) {
            $data['errors'] = $e->getMessage();
        }

		// Build and display view
        return Redirect::to('user/login')
        		->with_input()
        		->with('errors', $data['errors']);
    }

    /**
     * Do logout
     *
     * @return void
     */
    public function get_logout()
    {
    	// Logout user
        Sentry::logout();
		
		// Build and display view
        return Redirect::to('user/login');
    }
	
	/**
     * Reset password page
     *
     * @return \Laravel\View
     */
    public function get_reset()
    {
    	// Build and display view
        return View::make('user.reset');
    }

	/**
     * Send reset password email
     *
     * @return \Laravel\Redirect
     */
    public function post_reset()
    {
        // data pass to the view
        $data = array();

        // do valiation
        $rules = array(
            'email' => 'required|email',
        );
        $input = Input::get();
        $validation = Validator::make($input, $rules);

		/* Validation failed */
        if ($validation->fails()) {
            return Redirect::to('user/reset')
            		->with_input()
            		->with_errors($validation);
        }

		// Get user object
		$check_user = DB::table('users')
						->where('email', '=', Input::get('email'))
						->get();
		
		/* Users exists and doesn't have blank email */
		if (isset($check_user[0]) && $check_user[0]->email != '') {
			
			// Generate reset password link
        	try {
            	$reset = Sentry::reset_password(Input::get('email'), Member::randomPassword());
           		if (!$reset) {
            		$data['errors'] = 'There was an issue when reset the password';
           		}
     		}
       	 	catch (Sentry\SentryException $e) {
           		$data['errors'] = $e->getMessage();
        	}
		}
		/* No user with the given email */
		else {
			$data['errors'] = 'No user is registered with the email address <b>' . Input::get('email') . '</b>';
		}

		/* Show errors and abort proccess if errors exist */
        if ( array_key_exists ('errors', $data)) {
            return Redirect::to_secure('user/reset')
					->with('message', $data['errors'])
					->with('mode', 'danger');
        }
		/* No errors, send email */
        else {
        	// Build email content objects (with reset password link)
        	$content_object =  URL::base() . '/user/confirmation/' . $reset['link'];
			$email = new Mailer();
            $site_domain = preg_replace('#^https?://#', '', URL::base());
			$user_objects = array(
				'user' => $check_user[0],
				'system' => array('email' => "noreply@".$site_domain), // Statically coded, as generic noreply address
			);
			
			// Send email
			$email->predefinedEmail('resetpassword', $content_object, $user_objects, array(
				'user', 'system'
			));
			
			// Build and return
            return Redirect::to_secure('user/reset')
					->with('message', "<span>Password reset instructions have been sent.</span>")
					->with('mode', 'success');
        }
    }

	/**
     * Reset password link clicked
     *
	 * @param string $email Email Address (Base64 encrypted)
	 * @param string $hash Secret phrase (Base64 encrypted)
     * @return \Laravel\Redirect
     */
    public function get_confirmation($email = null, $hash = null)
    {
        try {
        	// Confirm password
            $confirmation = Sentry::reset_password_confirm($email, $hash);

			/* Confirmation link clicked, show form to enter new password */
            if ($confirmation) {
            	// Get user object
            	$safe_user = DB::table('users')
            					->where('email', '=', base64_decode($email))
            					->get();
								
				// Set session variables
            	Session::put('reset', $email . "/" . $hash);
				Session::put('user_id', $safe_user[0]->id);
				
				// Build and return view
				return Redirect::to_secure('user/reset')
						->with('mode', 'success')
						->with('message', 'Password confirmation link accepted.');
            }
            else {
            	// Confirmation link clicked but failed (expired or invalid)
            	return Redirect::to_secure('user/login')
						->with('mode','danger')
						->with('message','Unable to reset password');
            }
        }
        catch (Sentry\SentryException $e) {
            echo $e->getMessage();
        }
    }
	
	/**
     * New password set
     *
     * @return \Laravel\Redirect
     */
	public function post_confirmation()
	{
		// do valiation
        $rules = array(
            'password' => 'required|alpha_num',
            'password-confirm' => 'required|alpha_num',
        );
        $input = Input::get();
        $validation = Validator::make($input, $rules);

		/* Validation failed */
        if ($validation->fails()) {
            return Redirect::to('user/reset')->with_input()->with_errors($validation);
        } 
        /* Validation successful */
        else {
			/* Data ownership is correct */
       		if (Session::has('reset') && Input::has('reset')) {
        		/* Data integrity is viable */
        		if ( base64_decode( Session::get('reset') ) == base64_decode( Input::get('reset') ) 
        		&& Input::get('password') == Input::get('password-confirm')) {
					
					// Update users password, with new one given
        			$password = Input::get('password');
					if (Sentry::user_exists(intval(Session::get('user_id')))) {
        				$user = Sentry::user(intval(Session::get('user_id')));
					
						/* User information updated */
    					if ($user->update( array('password' => Input::get('password')))) {
       						// Build and return view
       						Session::flush();
        					return Redirect::to_secure('user/login')
									->with('message', 'Password reset sucessfully, you can now login using your new password')
									->with('mode', 'success');
   						}
   						else {
   							// Return fatal error
							Session::flush();
							die("Fatal server error");
						}
					}
					else{
						// Disabled or deleted user, reset the session
						Session::flush();
						return Redirect::to_secure('user/login')
							->with('message', 'User does not exist')
							->with('mode', 'danger');
					}
        		}
				/* Data integrity fail */
        		else {
					Session::flush();
					return Redirect::to_secure('user/login')
							->with('message', 'The password confirmation did not match, or password reset link was invalid/expired')
							->with('mode', 'danger');
				}
       		}
			/* Data ownership fail */
       		else {
				Session::flush();
				return Redirect::to_secure('user/login')
						->with('message', 'You do not have permission to change this users password')
						->with('mode', 'danger');
			}
		}
	}

	 /**
     * Do Delete
     *
	 * @param int $id User ID
     * @return \Laravel\Redirect
     */
	 public function get_delete($id)
	 {
	 	/* User exists */
	 	if (Sentry::user_exists( intval($id) ) != 1) {
	 		// Build and return view
			return Redirect::to_secure('member/list')
					->with('message', 'User you are trying to delete does not exist')
					->with('mode', 'danger');
		}
		
		// Get user perms
		$user = Session::get( Config::get('sentry::sentry.session.user'));
	    $user_group = Sentry::user($user);
		
		if (Sentry::group($user_group->group_id)
			->get('name')!='Admin') {
			// Build and return view
			return Redirect::to_secure('member/list')
					->with('message', 'You dont have permissions to delete the members')
					->with('mode', 'danger');
		}
		
		// Get user instances
	 	$userid = intval($id);
		$user = Sentry::user($userid);

        // Delete users permissions
        Member::removeCatPerms($userid);

        // Delete user
		$delete = $user->delete();  
		
		// Build and return view
		return Redirect::to_secure('member/list')
				->with('message', 'Member deleted.')
				->with('mode', 'danger');
    }

   /**
     * Do Edit
     *
     * @param int $id User ID 
     * @return mixed
     */
	public function get_edit($id) {
		/* User exists */
		if (Sentry::user_exists( intval($id)) != 1){
			return Redirect::to_secure('member/list')
					->with('message', 'User you are trying to edit does not exist')
					->with('mode', 'danger');
		}
		// Build user object
		$userid = intval($id);
		$new_user = Sentry::user($userid);
		
		// Get user perms
	 	$user = Session::get(Config::get('sentry::sentry.session.user'));
	 	$user_group = Sentry::user($user);
		
		/* User permissions valid */
		if (Sentry::group( $user_group->group_id)->get('name') != 'Admin' && Sentry::user()->id != $id) {
			return Redirect::to_secure('member/list')
					->with('message', 'You dont have permissions to access the editing members')
					->with('mode', 'danger');
		}
		/* User logged in */
		if (Sentry::check()) {

            // Check if user is admin (used to load certain UI only for admin)
            if (Sentry::group($user_group->group_id)->get('name') == 'Admin') {
                $is_admin = true;
            }
            else {
                $is_admin = false;
            }

			return View::make('user.edit_registration')
                        ->with('categories', Category::all())
			  			->with('members', $new_user['user'])
                        ->with('is_admin', $is_admin);
		}
		/* User not logged in */
		else {
			return Redirect::to('user/login');
		}		
	}
	
	 /**
     * Do Update
     *
     * @param int $id User ID 
     * @return \Laravel\Redirect
     */	
	public function post_edit($id)
	{
		/* Check if user exists */
		if (Sentry::user_exists( intval($id)) != 1) {
			return Redirect::to_secure('member/list')
					->with('message', 'User you are trying to edit does not exist')
					->with('mode', 'danger');
		}
		
		// Get user perms
		$user = Session::get( Config::get('sentry::sentry.session.user'));
   	    $user_group = Sentry::user($user);
		
		/* Permissions valid */
		if (Sentry::group($user_group->group_id)->get('name') != 'Admin' && Sentry::user()->id != $id) {
			return Redirect::to_secure('member/list')
					->with('message', 'You dont have permissions to access the editing members')
					->with('mode', 'danger');
		}
		
		// do valiation
		$data = array();
        $rules2 = array(
			 'name' => 'required',
			 'password' => 'confirmed',
             'email' => 'required'
           
        );

		// Make validation objects
        $input = Input::get();	
        $validation = Validator::make($input, $rules2);
		$checkemail = Member::editvalidate(Input::all(), $id);
		
        if ( count($checkemail) == 1) {
			return Redirect::to('user/edit/'.$id)
					->with('message','The Email Already Taken');
		}
      	else if ($validation->fails()){
            return Redirect::to('user/edit/'.$id)
            		->with_input()
            		->with_errors($validation);
        }

        try {
			$checkbox = Input::get('inlineCheckbox1');
			if ($checkbox == "") {
				$status='1';
			}
			else {
				$status='0';
			}
			$password = Input::get('password');
			
			if ($password != '') {
				$user = Sentry::user()->updating_pwd($id, array(
                	'email' => Input::get('email'),
                	'password' => Input::get('password'),
					'username' => Input::get('name'),
					'group_id' => Input::get('role'),
					'status' => $status
           		));
			}
			else {
            	$user = Member::update($id, array(
                	'email' => Input::get('email'),
					'username' => Input::get('name'),
					'group_id' => Input::get('role'),
					'status' => $status
            	));
			}

            if (!$user) {
                $data['errors'] = 'There was an issue when update user to database';
            }
        }
        catch (Sentry\SentryException $e) {
            $data['errors'] = $e->getMessage();
        }
        if ( array_key_exists('errors', $data)){
            return Redirect::to('user/edit/'.$id)
            		->with_input()
            		->with('errors', $data['errors']);
        }
        else {

            /* Process initial permissions for users to view (if admin) */
            if (Sentry::group($user_group->group_id)->get('name') == 'Admin') {
                foreach(Category::all() as $category) {

                    $viewing_permitted = (int)Input::get('Category'.($category->id), '0');

                    // Allow the member to be permitted to view current category (if not already)
                    if ((boolean)$viewing_permitted) {
                        if (!Member::hasCatPerms($id, $category->id)) {
                            Member::addCatPerms($id, $category->id);
                        }
                    }
                    // Remove permissions for anyone whose permissions have been revoked
                    elseif(Member::hasCatPerms($id, $category->id)) {
                        Member::removeCatPerms($id, $category->id);
                    }
                }
            }

        	if (Sentry::user()->id == $id) {
        		return Redirect::to_secure('document')
						->with('message', 'Changed your user details successfully')
						->with('mode', 'success');
        	}
			else {
				return Redirect::to_secure('member/list')
						->with('message', 'Member edited succesfully.')
						->with('mode', 'success');
			}
        }
    }
}
