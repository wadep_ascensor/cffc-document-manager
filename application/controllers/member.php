<?php

class Member_Controller extends Base_Controller {
	/**
	 * Controller implementes RESTful interface
	 *
	 * @var bool
	 */
    public $restful = true;

    /**
     * Show account information
     *
     * @return void
     */
    public function get_account()
    {
  		/* Has admin access */
	    if (Sentry::user()->has_access('admin')) {
			echo 'user has admin access';
		}
		/* Does not have admin access */
		else {
			echo 'user does not have admin access';
		}
    }
	
    /**
     * Show members list
     *
     * @return mixed
     */	 
 	public function get_list() 
 	{
 		// Get perms
		$user = Session::get(Config::get('sentry::sentry.session.user'));
		$users = Sentry::user($user);
		
		/* User is logged in */
		if(Sentry::check()) {
		    $per_page=20;
			$user = Sentry::user()->all();  
			$orders = Paginator::make($user, count($user), $per_page);
			$pages = Member::pagenavquery($per_page);
			return View::make('user.list')
					->with('orders',$orders)
					->with('groupid_login',$users->group_id)
					->with('members',$pages);
		}
		/* User is not logged in */
		else {
			 return Redirect::to('user/login');
		}		
	}
}
