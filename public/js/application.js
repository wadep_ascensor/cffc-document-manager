// Load jQuery before this script is loaded
$(document).ready(function(){
	/* JavaScript functionality shared across application */
	var original_form_url = $('form').attr('action'); // Make the default action URL reusable

	/* Add/Edit document description RTE */
	tinymce.init({
        mode : "textareas", 
        editor_selector :"mceEditor"
    });
	
	/* Category choosen */
	$("select[name=catfilter]").change( function(){
		$('form').attr('action', original_form_url);
		$("form").submit();
	});
	
	/* Delete document button clicked */
	$(".delete-document").click( function(){
		// Change the form action URL to match the link URL
		$('form').attr('action', $(this).attr('href'));

		// Bring up delete "Are you sure" prompt
		var confirm_prompt = "You sure you want to delete this record? Associated comments and file upload will also be deleted";
		var confirm_delete = confirm(confirm_prompt);
		if(confirm_delete == true){
			$('form').submit();
		}
		return false;
	});
	
	/* Delete comment button clicked */
	$(".delete-comment").click( function(){
		// Bring up delete "Are you sure" prompt
		var confirm_prompt = "You sure you want to delete this Comment?";
		var confirm_delete = confirm(confirm_prompt);
		if(confirm_delete == true){
			$('input#delete-comment').val($(this).val());
			$('form').submit();
		}
		return false;
	});
	
	/* Delete member button clicked */
	$(".delete-member").click( function(){
		// Bring up delete "Are you sure" prompt
		var confirm_prompt = "You sure you want to delete this Member?";
		var confirm_delete = confirm(confirm_prompt);
		if(confirm_delete == true){
			return true;
		}
		return false;
	});

	/* Popovers */
	// Intialize
	$('a[rel="popover"]').each(function() {
		$(this).popover({
			trigger: 'manual',
			html: true,
      		content: true
    	});
	});
	// Trigger (Using jQuery to avoid bugs)
	$('a[rel="popover"]').mouseover( function(){
		$(this).popover('show');
    });
    $('a[rel="popover"]').mouseleave( function(){
		$(this).popover('hide');
    });

    /* Block a comment submission that doesn't have a comment */
    $('#submit-comment').click( function(){
    	if($.trim($('#comment-text').val()) == ""){
    		alert("Please enter a comment before submitting");
    		return false;
    	}
    	else{
    		return true;
    	}
    });
});